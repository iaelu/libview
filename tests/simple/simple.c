#include <stdio.h>

#include "libview.h"

static void testview_renderer(vt_view *view, const char *buffer, size_t buflen,
                              void *userdata);

int main(void) {
  vt_view *view;
  int err;

  view = vt_view_create("testview.txt", &err);
  if (view == NULL) {
    fprintf(stderr, "err: %d\n", err);
    return (1);
  }

  if (vt_view_render(view, testview_renderer, NULL, 0, NULL) != 0) {
    vt_view_free(view);
    return (2);
  }

  vt_view_free(view);
  return (0);
}

static void testview_renderer(vt_view *view, const char *buffer, size_t buflen,
                              void *userdata) {
  /* print somewhere */
  const char *name = vt_view_get_name(view);
  fprintf(stderr, "view: %s, name: %s\n", vt_view_get_filename(view),
          name != NULL ? name : "<null>");
  fflush(stderr);
  fwrite(buffer, sizeof(char), buflen, stdout);
  fflush(stdout);
}
