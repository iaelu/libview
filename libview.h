/*-
 * Copyright (c) 2018, Guillaume Bibaut.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. Neither the name of Guillaume Bibaut nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Guillaume Bibaut BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __LIBVIEW_H__
#define __LIBVIEW_H__

#include <sys/types.h>

typedef struct vs_view vt_view;
typedef struct vs_mvp vt_mvp;

typedef void (*vf_writer)(vt_view *view, const char *buf, size_t buflen,
                          void *userdata);

typedef int (*vf_presenter)(vt_view *view, void *userdata);
typedef int (*vf_dataserv)(vt_view *view, void *userdata);
typedef void (*vf_freedata)(vt_view *view, void *userdata);

struct vs_mvp {
  char *viewname; /* required : view name, "default" for views which should
                     act on default workflow */
  vf_presenter presenter; /* required : a function for the view presentation */
  vf_dataserv dataserv;   /* optional : a function for the view data service */
  vf_freedata freedata;   /* optional : a function to free the data from the
                             data service */
};

vt_view *vt_view_create(const char *filename, int *err);
void vt_view_free(vt_view *view);

void vt_view_set_data(vt_view *view, void *data);
void *vt_view_get_data(vt_view *view);
void vt_view_set_render(vt_view *view, char *render);
const char *vt_view_get_filename(vt_view *view);
const char *vt_view_get_name(vt_view *view);

/*
 * Renders from the root view down to childrens
 */
int vt_view_render(vt_view *view, vf_writer writer, vt_mvp *mvps,
                   size_t mvps_count, void *userdata);

#endif /* __LIBVIEW_H__ */
