/*-
 * Copyright (c) 2018, Guillaume Bibaut.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 4. Neither the name of Guillaume Bibaut nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL Guillaume Bibaut BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "libview.h"

#define VT_ROOTVIEW_NAME "_rootview"

struct vs_view {
  char *file;      /* required: view filename */
  char *_filename; /* internal */

  char *name; /* optional: view name, there shall be a MVP with viewname = */
              /* "default" */

  off_t declstart; /* optional: position of the view declaration, 0 for the */
                   /* default view */
  size_t declsize; /* optional: size of the view declaration, 0 for the */
                   /* default view */

  void *data;
  char *render;
};

extern vt_view *vt_view_find(const char *buffer, int *err);

/* default helper if none specified */
static int _default_presenter(vt_view *view, void *userdata);
static vt_mvp _default_mvp = {"default", _default_presenter, NULL, NULL};
static vt_mvp *vt_mvp_find(vt_mvp *mvps, size_t mvps_count, vt_view *view);
static int get_document_root(char *root);
static int get_document_alias(char *aliasuri, char *aliasroot);

vt_view *_vt_view_create(const char *viewfile, const char *viewname,
                         off_t declstart, size_t declsize, int *err);

/*
 * a default presenter fonction which just read the content of a
 *  file and returns it
 */
static int _default_presenter(vt_view *view, void *userdata) {
  FILE *fview;
  struct stat st;

  if (stat(view->_filename, &st) != 0) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] could not stat\n", __func__, __LINE__);
#endif
    return (-1);
  }

  if ((fview = fopen(view->_filename, "r")) == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] could not open\n", __func__, __LINE__);
#endif
    return (1);
  }
  view->render = calloc(st.st_size + 1, sizeof(char));
  if (view->render == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] alloc failed\n", __func__, __LINE__);
#endif
    return (2);
  }
  view->render[st.st_size] = '\0';
  fread(view->render, 1, st.st_size, fview);
  fclose(fview);

  return (0);
}

static int get_document_root(char *root) {
  char *env;
  /* Apache 2.x VirtualDocumentRoot bypass, feature */
  if ((env = getenv("VIEWS_ROOT")) != NULL) {
    if ((realpath(env, root)) != NULL) {
      return (0);
    }
  }
  /* DOCUMENT_ROOT */
  if ((env = getenv("DOCUMENT_ROOT")) != NULL) {
    if (realpath(env, root) != NULL) {
      return (0);
    }
  }
  /* fallback to current directory */
  if (realpath(".", root) != NULL) {
    return (0);
  }
  return (1);
}

static int get_document_alias(char *aliasuri, char *aliasroot) {
  char *docalias, *orig;
  char *ptr;
  if (getenv("DOCUMENT_ALIAS") == NULL) {
    return (-1);
  }
  orig = docalias = strdup(getenv("DOCUMENT_ALIAS"));
  if (docalias == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] alloc failed\n", __func__, __LINE__);
#endif
    return (1);
  }
  if ((ptr = strsep(&docalias, ",")) == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] DOCUMENT_ALIAS\n", __func__, __LINE__);
#endif
    free(orig);
    return (2);
  }
  strcpy(aliasuri, ptr);
  if ((ptr = strsep(&docalias, ",")) == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] DOCUMENT_ALIAS\n", __func__, __LINE__);
#endif
    free(orig);
    return (3);
  }
  if (realpath(ptr, aliasroot) == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] realpath failed\n", __func__, __LINE__);
#endif
    free(orig);
    return (4);
  }
  free(orig);
  return (0);
}

/*
 * create a view's construct
 */
vt_view *_vt_view_create(const char *viewfile, const char *viewname,
                         off_t declstart, size_t declsize, int *err) {

  vt_view *view;
  char aliasuri[MAXPATHLEN + 1];
  char aliasdir[MAXPATHLEN + 1];
  char rootdir[MAXPATHLEN + 1];
  size_t aliasurisz;
  char *ptr;
  int ret;

  *err = 0;

  if (viewfile == NULL || *viewfile == '\0') {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] no filename\n", __func__, __LINE__);
#endif
    *err = -1;
    return ((vt_view *)NULL);
  }

  view = calloc(1, sizeof(vt_view));
  if (view == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] alloc failed\n", __func__, __LINE__);
#endif
    *err = 1;
    return ((vt_view *)NULL);
  }
  view->file = strdup(viewfile);
  if (view->file == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] alloc failed\n", __func__, __LINE__);
#endif
    vt_view_free(view);
    *err = 2;
    return ((vt_view *)NULL);
  }
  if (viewname != NULL) {
    view->name = strdup(viewname);
    if (view->name == NULL) {
#if defined(DEBUG)
      fprintf(stderr, "[%s:%d] alloc failed\n", __func__, __LINE__);
#endif
      *err = 3;
      vt_view_free(view);
      return ((vt_view *)NULL);
    }
  }
  view->declstart = declstart;
  view->declsize = declsize;

  /* process view file */
  /* DOCUMENT_ALIAS */
  if ((ret = get_document_alias(aliasuri, aliasdir)) > 0) {
    *err = 5;
    vt_view_free(view);
    return (NULL);
  }
  if (ret == 0) {
    if (strstr(view->file, aliasuri) != view->file) {
      ret = -1;
    } else {
      aliasurisz = strlen(aliasuri);
      if (asprintf(&(view->_filename), "%s/%s", aliasdir,
                   &(view->file[aliasurisz])) == -1) {
#if defined(DEBUG)
        fprintf(stderr, "[%s:%d] alloc failed\n", __func__, __LINE__);
#endif
        *err = 6;
        vt_view_free(view);
        return (NULL);
      }
    }
  }
  /* DOCUMENT_ROOT */
  if (ret < 0) {
    if (get_document_root(rootdir) != 0) {
      *err = 6;
      vt_view_free(view);
      return (NULL);
    }
    if (asprintf(&(view->_filename), "%s/%s", rootdir, view->file) == -1) {
#if defined(DEBUG)
      fprintf(stderr, "[%s:%d] alloc failed\n", __func__, __LINE__);
#endif
      *err = 7;
      vt_view_free(view);
      return (NULL);
    }
  }
  if ((ptr = realpath(view->_filename, NULL)) == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] alloc failed, file: %s, _filename: %s\n", __func__,
            __LINE__, view->file, view->_filename);
#endif
    *err = 8;
    vt_view_free(view);
    return (NULL);
  }
  free(view->_filename);
  view->_filename = ptr;

  /* security verifications */
  /* not a secured path ? */
  if (strlen(view->_filename) > MAXPATHLEN ||
      strstr(view->_filename, "/etc/") == view->_filename) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] unsecured path\n", __func__, __LINE__);
#endif
    *err = 9;
    vt_view_free(view);
    return (NULL);
  }

  return (view);
}

/*
 * finds the MVP functions for a view, if any, if not returns a default set
 * of MVP functions
 */
static vt_mvp *vt_mvp_find(vt_mvp *mvps, size_t mvps_count, vt_view *view) {
  size_t index;

  if (view->name != NULL) {
    for (index = 0; index < mvps_count; index++) {
      if (strcmp(mvps[index].viewname, view->name) == 0) {
        return (&(mvps[index]));
      }
    }
  } else {
    for (index = 0; index < mvps_count; index++) {
      if (strcmp(mvps[index].viewname, "default") == 0) {
        return (&(mvps[index]));
      }
    }
  }
  return (&_default_mvp);
}

/*
 * create the root view
 */
vt_view *vt_view_create(const char *filename, int *err) {
  vt_view *view;
  if ((view = _vt_view_create(filename, VT_ROOTVIEW_NAME, 0, 0, err)) == NULL) {
    return (NULL);
  }
  return (view);
}

/*
 * deallocate a view
 */
void vt_view_free(vt_view *view) {

  if (view == NULL) {
    return;
  }
  if (view->file != NULL) {
    free(view->file);
  }
  if (view->name != NULL) {
    free(view->name);
  }
  if (view->_filename != NULL) {
    free(view->_filename);
  }
  free(view);
}

/*
 * renders a view with its associated set of MVP functions
 */
int vt_view_render(vt_view *view, vf_writer writer, vt_mvp *mvps,
                   size_t mvpscount, void *userdata) {
  vt_mvp *mvp;
  vt_view *childview;
  char *buffer;
  int ret;

  /* find the functions associated with the view */
  mvp = vt_mvp_find(mvps, mvpscount, view);
  if (mvp == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] mvp error view: '%s'\n", __func__, __LINE__,
            view->file);
#endif
    return (1);
  }
  view->data = NULL;
  if (mvp->dataserv != NULL) {
    if (mvp->dataserv(view, userdata) != 0) {
      return (2);
    }
  }
  /* call the presenter function, with the view data if any */
  view->render = NULL;
  if (mvp->presenter) {
    if (mvp->presenter(view, userdata) != 0) {
      if (view->data != NULL && mvp->freedata != NULL) {
        mvp->freedata(view, userdata);
      }
      return (3);
    }
  }
  if (view->render == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] presenter error, view: '%s'\n", __func__, __LINE__,
            view->file);
#endif
    if (view->data != NULL && mvp->freedata != NULL) {
      mvp->freedata(view, userdata);
    }
    return (4);
  }

  /* Let's see if there are views defined in the current view buffer */
  buffer = view->render;
  childview = vt_view_find(buffer, &ret);
  if (ret != 0) {
    if (view->data != NULL && mvp->freedata != NULL) {
      mvp->freedata(view, userdata);
    }
    return (10 + ret);
  }
  while (childview != NULL) {
    /* at least let's renderer what can be already render before we try
     * to render the child view */
    writer(view, buffer, childview->declstart, userdata);
    /* Now let's render that child view (recursive call) */
    if ((ret = vt_view_render(childview, writer, mvps, mvpscount, userdata)) !=
        0) {
      vt_view_free(childview);
      if (view->data != NULL && mvp->freedata != NULL) {
        mvp->freedata(view, userdata);
      }
      return (20 + ret);
    }
    /* move at the end of the child view declaration */
    buffer += (childview->declstart + childview->declsize);
    vt_view_free(childview);
    /* Let's see if there are views defined in the current view buffer
     * after that child view */
    childview = vt_view_find(buffer, &ret);
    if (ret != 0) {
      if (view->data != NULL && mvp->freedata != NULL) {
        mvp->freedata(view, userdata);
      }
      return (30 + ret);
    }
  }
  /* then renderer what's left if there are no more children views */
  writer(view, buffer, strlen(buffer), userdata);
  if (view->data != NULL && mvp->freedata != NULL) {
    mvp->freedata(view, userdata);
  }
  free(view->render);
  return (0);
}

void vt_view_set_data(vt_view *view, void *data) { view->data = data; }

void *vt_view_get_data(vt_view *view) { return view->data; }

void vt_view_set_render(vt_view *view, char *render) { view->render = render; }

const char *vt_view_get_filename(vt_view *view) { return view->file; }

const char *vt_view_get_name(vt_view *view) { return view->name; }
