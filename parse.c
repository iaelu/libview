#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "libview.h"

extern vt_view *_vt_view_create(const char *viewfile, const char *viewname,
                                off_t declstart, size_t declsize, int *err);

/*
 * tries to find a child view inside a buffer and
 * returns it with its metadata
 */
vt_view *vt_view_find(const char *buffer, int *err);

/*
 * template strings
 */
const char *leftdelim = "<!--";
const char *rightdelim = "-->";
const char *includetag = "#include";
const char *virtualdecl = "virtual=\"";
const char *nametag = "#viewname=\"";

enum _itemtype {
  VIEW_NONE = 0,
  VIEW_INCLUDE = 1,
  VIEW_NAME = 1 << 1,
};
typedef enum _itemtype itemtype;
#define HAS_VIEWINCLUDE(s) ((s & VIEW_INCLUDE) != 0)
#define HAS_VIEWNAME(s) ((s & VIEW_NAME) != 0)

/*
 * tries to find a child view inside a buffer and
 * returns it with its metadata
 */
vt_view *vt_view_find(const char *buffer, int *err) {
  off_t declstart;
  size_t declsize, tmpsz;
  const char *ptr;
  char *startptr, *endptr, **var;
  char *viewfile, *viewname;
  itemtype state, tmpstate;
  vt_view *view;

  *err = 0;
  if (buffer == NULL) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] null argument\n", __func__, __LINE__);
#endif
    *err = -1;
    return (NULL);
  }

  declstart = 0;
  declsize = 0;
  viewfile = NULL;
  viewname = NULL;

  ptr = buffer;
  state = VIEW_NONE;
  while ((startptr = strstr(ptr, leftdelim)) != NULL) {
    ptr = startptr + strlen(leftdelim);
    while (isspace(*ptr)) {
      ptr++;
    }

    var = NULL;
    tmpstate = VIEW_NONE;
    if ((state & VIEW_INCLUDE) == 0) {
      if (strstr(ptr, includetag) != ptr) {
        continue;
      }
      ptr += strlen(includetag);
      while (isspace(*ptr)) {
        ptr++;
      }
      if (strstr(ptr, virtualdecl) != ptr) {
        continue;
      }
      tmpstate = VIEW_INCLUDE;
      ptr += strlen(virtualdecl);
      declstart = (off_t)(startptr - buffer);
      var = &viewfile;
    } else if ((state & VIEW_NAME) == 0) {
      if (strstr(ptr, nametag) == ptr) {
        tmpstate = VIEW_NAME;
        ptr += strlen(nametag);
        var = &viewname;
      }
    }
    if (tmpstate == VIEW_NONE) {
      continue;
    }

    if ((endptr = strchr(ptr, '"')) == NULL) {
      continue;
    }
    tmpsz = (size_t)(endptr - ptr);
    *var = (char *)calloc(tmpsz + 1, sizeof(char));
    if ((*var) == NULL) {
#if defined(DEBUG)
      fprintf(stderr, "[%s:%d] alloc failed\n", __func__, __LINE__);
#endif
      if (viewfile != NULL) {
        free(viewfile);
      }
      if (viewname != NULL) {
        free(viewname);
      }
      *err = 1;
      return (NULL);
    }
    memcpy(*var, ptr, tmpsz);
    ptr = endptr + 1;

    while (isspace(*ptr)) {
      ptr++;
    }
    if (strstr(ptr, rightdelim) != ptr) {
      free(*var);
      *var = NULL;
      continue;
    }
    ptr += strlen(rightdelim);
    declsize = (size_t)(ptr - &(buffer[declstart]));
    state |= tmpstate;
  }

  if (state == VIEW_NONE) {
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] no view found\n", __func__, __LINE__);
#endif
    if (viewfile != NULL) {
      free(viewfile);
    }
    if (viewname != NULL) {
      free(viewname);
    }
    return (NULL);
  }

  /* create a view, with its filename, name and index */
  if ((view = _vt_view_create(viewfile, (HAS_VIEWNAME(state)) ? viewname : NULL,
                              declstart, declsize, err)) == NULL) {
    *err += 100;
#if defined(DEBUG)
    fprintf(stderr, "[%s:%d] creation error: %d\n", __func__, __LINE__, err);
#endif
  }
  if (viewfile != NULL) {
    free(viewfile);
  }
  if (viewname != NULL) {
    free(viewname);
  }
  return (view);
}
