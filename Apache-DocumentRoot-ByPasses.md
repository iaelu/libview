This library can use the DOCUMENT_ROOT environment variable set by Apache. It is one of the default in that library.
The other possible use is when a VirtualHost can be a root HTML hierarchy, but also has some aliases to other folders. To that goal, I've decided to use an Apache environment variable that can be set with some configuration. The env. var. has to be named DOCUMENT_ALIAS, and for now it can be set with such config in Apache :

```apache
<VirtualHost *:80>
[...]
    DocumentRoot "/var/www"
[...]
    Alias /an_alias /var/www_alias_folder
    SetEnvIf Request_URI ^/an_alias/.*\.html$ DOCUMENT_ALIAS=/an_alias,/var/www_alias_folder
[...]
</VirtualHost>
```
which means here that we are securing an alias folder to be used by a CGI using libview to only `*.html` files that has to be in this peculiar folder.

---

Apache 2.2 VirtualDocumentRoot feature, and DocumentRoot overwriting:
version 2.2 of Apache, and until recent Apache 2.4, do not overwrite DOCUMENT_ROOT environnement variable. The VIEWS_ROOT enables libview to overwrite DOCUMENT_ROOT for CGI executables.

```apache
<VirtualHost *:80>
[...]
    ServerName none.domain.com
    ServerAlias *.domain.com
    DocumentRoot "/path/to/html"
    VirtualDocumentRoot /path/to/%1/html
[...]
    SetEnvIf Host ^([^\.]+)\.domain\.com$ VIEWS_ROOT=/path/to/$1/html/
[...]
</VirtualHost>
```

