# libview

## Simple and specialized Model View Presenter (MVP) library

This library is a very specialized MVP implementation in C.
This library has been developed on FreeBSD and is mainly designed to work on FreeBSD.
This has not been tested on Linux, but since the code is quite simple, this should work on Linux.

It is very specialized because it's replacing a SSI (Server Side Include) feature.
There is an inherent workflow that tells the library to render the result of the MVP, rendering part of the parent view before the child view, then the child view, back to the rest of the parent view.

See [test](test) for a sample.

There are also 2 features available to bypass Apache `{Virtual,}DocumentRoot` configuration:
[Apache DocumentRoot ByPasses](Apache-DocumentRoot-ByPasses.md)