LIB= view

SRCS+= libview.c
SRCS+= parse.c

WARNS?=2

uname != uname
.if ${uname} != FreeBSD
NO_MAN=1
NO_PROFILE=1
NO_OBJ=1
NO_WERROR=1

CFLAGS+= -D_GNU_SOURCE
.else
MK_MAN=no
MK_PROFILE=no
NO_OBJ=1
MK_WERROR=no
MK_PIE=no
.endif

test: all
	@${MAKE} -C tests test

testclean: cleandir
	${MAKE} -C tests testclean

.include <bsd.lib.mk>
